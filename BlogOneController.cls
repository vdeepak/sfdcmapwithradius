global with sharing class BlogOneController {

    /*
    Sample Controller to view,create and delete account
    */
    
    // ATRRIBUTES
    
    

    // CONSTRUCTOR

    public BlogOneController(){

    }

    public BlogOneController(ApexPages.StandardController controller){

    }
    
    // METHODS
    
    /*
    Fetch all accounts with distance in match with location
    */


    @RemoteAction
    global static List<AccountWrapper> getAllAccountsWithInDistance(String latLng,Integer dist){
        List<AccountWrapper> wrapper = new List<AccountWrapper>();
        String query = 'SELECT Id,Name,Location__latitude__s, Location__longitude__s,BillingCity FROM Account WHERE DISTANCE(Location__c, GEOLOCATION('+latLng+'), \'km\') < '+dist;
        for(Account acc : Database.query(query)){
            wrapper.add(new AccountWrapper(acc));
        }
        return wrapper;
    }

/*
create tasks for all selected account

*/

    @RemoteAction 
    global static void createTasks(String dt,String sub,String com,List<String> accIds){

        try{
            List<Task> tasks = new List<Task>();
            for(String accId : accIds){
                tasks.add(new Task(Subject = sub,WhatId = accId, ActivityDate = Date.parse(dt),Status = 'In Progress',Priority = 'Normal',OwnerId = UserInfo.getUserId(),Description = com));
            }
            insert tasks;

        }catch(Exception e){
            system.debug(e.getMessage());
        }

       

    }

    /* Account Selection Wrapper */     


    global class AccountWrapper{
        public Boolean flag {get;set;}
        public Account account {get;set;}

        public AccountWrapper(Account acc){
            this.flag = true;
            this.account = acc;
        } 
    }


}